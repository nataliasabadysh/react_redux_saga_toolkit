
// Core
import { configureStore } from '@reduxjs/toolkit';
// Instruments
import { rootSaga } from './rootSaga';
import { middleware, sagaMiddleware } from './middleware';

// Instruments
import { swapiReducer as swapi } from '../bus/swapi/reducer';

const store = configureStore({
    reducer: {
        swapi
    },
    middleware: middleware
});
sagaMiddleware.run(rootSaga);

export { store };

