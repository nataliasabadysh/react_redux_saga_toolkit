/**
 * redux-saga также предоставляет возможность имплементировать очередь выполнения эффектов.
 */

// Core
import { take, call, actionChannel } from 'redux-saga/effects';

// Workers
import { fetchEntity } from './fetchEntity';
import { swapiActions } from '../../bus/swapi/actions';
export function* runExample() {
    const buffer = yield actionChannel(swapiActions.fetchAll.type);

    while (true) {
        const action = yield take(buffer);

        yield call(fetchEntity, action, 'Planets');
        yield call(fetchEntity, action, 'Vehicles');
        yield call(fetchEntity, action, 'People');
    }
}
