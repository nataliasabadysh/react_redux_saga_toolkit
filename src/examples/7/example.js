/**
 * Можно осуществлять даже более сложный контроль над параллельными задачами.
 */

// Core
import { take, fork, cancel, all } from 'redux-saga/effects';

// Instruments
import { swapiActions } from '../../bus/swapi/actions';

// Workers
import { fetchEntity } from './fetchEntity';

const tasks = [];

function* watchFetchPlanetsAsync() {
    while (true) {
        const action = yield take(swapiActions.fetchPlanetsAsync.type);
        const planetsTask = yield fork(fetchEntity, action, 'Planets');
        console.log(action);

        tasks.push(planetsTask);
    }
}

function* watchCancelFetch() {
    while (true) {
        yield take(swapiActions.cancelFetch.type);

        if (tasks.length) {
            for (const task of tasks) {
                yield cancel(task);
            }
            tasks.length = 0;
        }
    }
}

export function* watchFetchAll() {
    while (true) {
        const action = yield take(swapiActions.fetchAll.type);

        // 3 paralel fetching ... 
        const peopleTask = yield fork(fetchEntity, action, 'People');
        const vehiclesTask = yield fork(fetchEntity, action, 'Vehicles');
        const planetsTask = yield fork(fetchEntity, action, 'Planets');

        tasks.push(peopleTask);
        tasks.push(vehiclesTask);
        tasks.push(planetsTask);
    }
}

export function* runExample() {
    yield all([ watchFetchPlanetsAsync(), watchCancelFetch(), watchFetchAll() ]);
}
