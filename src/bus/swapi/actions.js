import { createAction } from '@reduxjs/toolkit';

export const swapiActions = {
    fetchAll: createAction('FETCH_ALL'),
    fetchVehiclesAsync: createAction('FETCH_VEHICLES_ASYNC'),    
    fillVehicles: createAction('FILL_VEHICLES'), 
    fetchPeopleAsync: createAction('FETCH_PEOPLE_ASYNC'),
    fillPeople: createAction('FILL_PEOPLE'),
    fetchPlanetsAsync: createAction('FETCH_PLANETS_ASYNC'),
    fillPlanets: createAction('FILL_PLANETS'),
    cancelFetch: createAction('CANCEL_FETCH'),
    setIsFetching: createAction('SET_IS_FETCHING'),
};
