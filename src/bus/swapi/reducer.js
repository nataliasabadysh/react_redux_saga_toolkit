import { createReducer, combineReducers } from '@reduxjs/toolkit';
// Instruments
import { swapiActions } from './actions';

const isFetching = createReducer(false, {
    [swapiActions.createAction]: (state, action) => action.payload, 
});
const vehicles = createReducer([], {
    [swapiActions.fillVehicles]: (state, action) => action.payload,
});
const people = createReducer([], {
    [swapiActions.fillPeople]: (state, action) => action.payload,
});
const planets = createReducer([], {
    [swapiActions.fillPlanets]: (state, action) => action.payload,
});

export const swapiReducer = combineReducers({
    isFetching,
    vehicles,
    people,
    planets
});
