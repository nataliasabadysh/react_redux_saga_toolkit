# Saga effects
_ function 

# take effect
_ takes string as type action for the monitoring 
_ blocking effect 

# put effect
_ same as dispatch 
_ started action redux in saga worker 

```
export function* runExample() {
    while (true) {
        const action = yield take(types.FETCH_PLANETS_ASYNC);

        yield put(swapiActions.setIsFetching(true));
        const response = yield api.fetchPlanets(action.payload); // page number 
        const data = yield response.json();

        yield put(swapiActions.fillPlanets(data.results)); // fill in reducer 
        yield put(swapiActions.setIsFetching(false));
    }

```
# call effect

_ to fire  the function 
_ just have diff sintacks
_! making block on this functions till it will finished  

# aplay effect
_ подхрдить для сохранения контексто выполнения 


```
export function* runExample() {
    while (true) {
        const action = yield take(types.FETCH_PLANETS_ASYNC);

        yield put(swapiActions.setIsFetching(true));
        const response = yield call(api.fetchPlanets, [ action.payload ]);
        const data = yield apply(response, response.json);

        yield put(swapiActions.fillPlanets(data.results));
        yield put(swapiActions.setIsFetching(false));
    }
}

```

# delay effect

```
yield delay(1000);
```


# 1. delegirovanie  with  yield*  

```
function* fetchPlanets(action) {
    const response = yield call(api.fetchPlanets, action.payload);
    const data = yield apply(response, response.json);

    return data;
}

export function* runExample() {
    while (true) {
        const action = yield take(types.FETCH_PLANETS_ASYNC);

        yield put(swapiActions.setIsFetching(true));
        const data = yield* fetchPlanets(action);

        yield put(swapiActions.fillPlanets(data.results));
        yield put(swapiActions.setIsFetching(false));
    }
}


```

# 2. delegirovanie  with  call

_ blocking effect !

```
function* fetchPlanets(action) {
    const response = yield call(api.fetchPlanets, action.payload);
    const data = yield apply(response, response.json);

    return data;
}

export function* runExample() {
    while (true) {
        const action = yield take(types.FETCH_PLANETS_ASYNC);

        yield put(swapiActions.setIsFetching(true));
        yield delay(1000);
        const data = yield call(fetchPlanets, action);

        yield put(swapiActions.fillPlanets(data.results));
        yield put(swapiActions.setIsFetching(false));
    }
}



```


# fork effect
- not blocking the affect  
- call is blocking  

```
// Instruments
import { types } from '../../bus/swapi/types';
import { swapiActions } from '../../bus/swapi/actions';
import { api } from '../../Api';

function* fetchPlanets(action) {
    const response = yield call(api.fetchPlanets, action.payload);
    const data = yield apply(response, response.json);

    yield put(swapiActions.fillPlanets(data.results));
}

export function* runExample() {
    while (true) {
        const action = yield take(types.FETCH_PLANETS_ASYNC);

        const task = yield fork(fetchPlanets, action);

        console.log('→ task', task);
    }
}

```



# cansel effect 


```

function* fetchPlanets(action) {
    try {
        yield delay(2000);

        const response = yield call(api.fetchPlanets, action.payload);
        const data = yield apply(response, response.json);

        yield put(swapiActions.fillPlanets(data.results));
    } catch (error) {
        console.log('→ error', error);
    } finally {
        if (yield cancelled()) {
            console.log('→ cancelled!', action.type);
        }
    }
}

export function* runExample() {
    const tasks = [];

    while (true) {
        const action = yield take([
            types.FETCH_PLANETS_ASYNC,
            types.CANCEL_FETCH,
        ]);

        if (tasks.length && action.type === types.CANCEL_FETCH) {
            for (const task of tasks) {
                yield cancel(task);
            }
            tasks.length = 0;

            continue;
        }

        const task = yield fork(fetchPlanets, action);

        tasks.push(task);

        console.log('→ tasks', tasks);
    }
}

```

# all effect 

```
export function* runExample() {
    yield all([ watchFetchPlanetsAsync(), watchCancelFetch(), watchFetchAll() ]);
}
```


# 3 fethcing in the same time example 7
